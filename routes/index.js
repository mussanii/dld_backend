const express = require('express')

const router = express.Router()

const userSignUpController = require('../controllers/userSignUp')
const userSigninController = require('../controllers/userSignin')

router.post("/signup",userSignUpController)
router.post("/signin", userSigninController)


module.exports = router